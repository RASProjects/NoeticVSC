# Noetic Visual Studio Code Development Container

Project under **active development**. Also, expect more templates like this for other ROS distributions.

![GitLab License](https://img.shields.io/gitlab/license/ROSProjects/NoeticVSC?color=gree&style=for-the-badge)
![GitLab contributors](https://img.shields.io/gitlab/contributors/ROSProjects/NoeticVSC?style=for-the-badge)

This repository consist on a template for using ROS Noetic within a devcontainer inside Visual Studio Code, with full access to the host network interfaces and display for tools like rqt.

## Using this template for a new project

1. Clone the repository
	```bash
	git clone https://gitlab.com/ROSProjects/NoeticVSC
	```
2. Rename the folder
	```bash
	mv NoeticVSC <your project name>
	```
3. Enter into the project folder
	```bash
	cd <your project name>
	```
4. Remove the .git folder
	```bash
	rm -rf .git
	```
5. Enter VSCode and Reopen in Container
	```bash
	code .
	```
6. Create a new catkin workspace
	```bash
	catkin_make
	```
7. Enter the source (src) folder
	```bash
	cd src
	```
8. Create a package
	```bash
	catkin_create_pkg <your package name> std_msgs rospy roscpp message_generation <more dependencies>
	```
9. Optionally, perform the [extra](#extra) step

10. Enter into the package folder
	```bash
	cd <your package name>
	```
11. Create a git repository
	```bash
	git init
	```

12. Open the package folder inside the VSCode Workspace

13. Save the VSCode Workspace

### Extra

Remove README.md, CONTRIBUTING.md and LICENSE to add your own. 

**Important:** You must include this license somewhere else in your project.

```bash
rm README.md CONTRIBUTING.md LICENSE
```

## Contributing

All contributions are welcome under the Contributor Covenant Code of Conduct. For more information see [CONTRIBUTING.md](./CONTRIBUTING.md)

## Authors and acknowledgment

- @JuanCSUCoder - Juan Camilo Sánchez Urrego

## License

Licensed under the MIT license. For more information see [LICENSE](./LICENSE)

